Welcome to this website! Here you can see the documentation of one of the students of the course _How To Make (almost)
Any Experiment Using Digital Fabrication_, taught at FabLab ULB (Université Libre de Bruxelles), in the 2nd semester of the academic year
2021/2022.


## About me

![](images/avatar-photo.jpg)

Hello! My name is Catarina Corte-Real, I am from Portugal and I am an exchange student at ULB for the Spring semester 2022.
I am a 1st year master student of engeneering physics at Instituto Superior Técnico, in Lisbon.


## My background

I was born in Braga, northern Portugal, in August 1999, and I moved to Lisbon when I was 18 to study in university.

When I was a child, I used to like reading adventure books all the time. Even as I grew older I continued to be a textbook nerd: I loved learning new
things, regardless of the area. I always loved maths, but it wouldn't be untill my late teenage years that I would find my biggest calling,
 though: the rawest understanding of nature (physics), the language we process it in (maths), and the use of this knowledge to, hopefully, better
 the world (engeneering).

## Previous work

In high school, me and my class participated in _Euroescola_, an iniciative of the European Parliament to bring young people from all European Union
member states to learn more about the EU, as well as simulate their own parliament session, discussing pressing issues of the world at the moment. It is
also a great oportunity to meet people from almost 30 nationalities in the beautiful city of Strasbourg.

Already in university, in the summer of 2020, I participated in a summer internship of a laboratory associated with my home faculty, [LIP](https://www.lip.pt/?section=home&page=homepage&lang=en) (in portuguese,
_**L**aboratório de **I**nstrumentação e Física Experimental de **P**artículas_, which translates to _Laboratory of Instrumentation
and Experimental Particle Physics_). The purpose of the project was to understand the known properties of a particle, J/ψ, and develop code to
select the corresponding production events in [CERN](https://home.cern/)'s experiment COMPASS (**CO**mmon **M**uon **P**roton **A**pparatus for
 **S**tructure and **S**pectroscopy) and study further its behaviour.
