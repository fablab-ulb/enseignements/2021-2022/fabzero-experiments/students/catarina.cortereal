# 1. Documentation

This week I worked on defining my final project idea and started to getting used to the documentation process.

Documentation is recording what was done and learnt in a project. It is important for various reasons; firstly, it allows the author to revisit their own process in case they need to repeat something, or verify it. It also means that other people who could want to contribute to the project, or inspire themselves for one of their own, can have concrete help in specific parts of the procedure. Lastly, it is an instrument for controling and acessing work; in fact, when methods of operating are recorded and accessible, both the author and other people can think of better and most effective ways of achieving the same goals, as well as even finding flaws to be corrected.


## 1.1 Using gitlab

[gitlab](https://gitlab.com) is a free online software repository, that allows people to contribute to code in groups and share projects. It has version control, which means that every version of the code is stored, even after changes. It is used in this course to create a website with the documentation of the different modules to be learnt, as well as the final project.

To use gitlab, one must firstly create an account. To be able to edit code in one's personal computer, it is necessary to clone it  and connect the computer to the server. In my case, this was done in Windows 10. Firstly, it should be verified if gitlab is already installed, by writing

```
git --version
```
Since it was already installed, I moved on to the next phase, which is stating the username and e-mail associated with the gitlab account.
```
git config --global user.name "your_username"
git config --global user.email "your_email_address@example.com"
```

Afterwards, this can be checked by writing

```
git config --global --list
```

Then, it is necessary to make the SSH (or HTTPS, although I used SSH) connection between the computer and the server.
An SSH key has to be created, if it does not already exist. This can be achieved by writing in the command line

```
ssh-keygen -t ed25519 -C "<comment>"
```
where the comment is optional. The public part of this key (the private should never be shared) needs to be shared with the gitlab account. This is done by clicking the right-top side of the screen button with your gitlab profile picture, and then selecting Preferences->SSH keys and then copying the key.
Afterwards, a link is generated to clone the code on gitlab, by going to the project's page, clicking the button "Clone", and copying the "Clone with SSH" URL. To have the project's folder, with all its files, in a given directory, one must then open a terminal there and do

```
git clone git@gitlab.com:gitlab-tests/sample-project.git
```
with the correct name of the project to be cloned.
The folder should appear in the computer after this is done. If so, one is then ready to edit the code in the computer. Once the editing is done, the following steps should be taken to upload the changes to gitlab.
```
git status
git add -A
git commit -m "COMMENT TO DESCRIBE THE INTENTION OF THE COMMIT"
```
The comment is stored and showed associated with the project's version. The "git status" line compares what is in the server with what is in the local copy, and lets  the user know what there is to commit.
Finally, one should write
```
git push
```
after which all changes should have been updated in the server.

The previous work was done by following gitlab documentation links:

-[Start using git on the command line](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#install-git)

-[Use SSH keys to communicate with GitLab](https://docs.gitlab.com/ee/ssh/index.html)

## 1.2 Image editing

Another important skill to better use the tools at our disposal is to edit images such that they occupy less space on the online servers. This constitutes a responsible use of GitLab, and it is important for the environment, because everything that is digitally stored uses the planets' resources. In fact, a single picture taken from a phone camera nowadays can weigh about 10MB!

There are several softwares that can be installed to reduce image (and video) size, such as [GraphicsMagick](http://www.graphicsmagick.org/), [ImageMagick](https://imagemagick.org/index.php) and [GIMP](https://www.gimp.org/). All of these work at least on Linux, Windows and Mac Os.

I chose to install GraphicsMagick, since it seemed fast and fairly simple to use, working on the command line. Since I have a Windows run computer with a Linux VirtualBox, I installed it on the later, because the steps are extremely simple.

### Installing GraphicsMagick on Ubuntu 20.04

First, as with any Linux command line installation, the following line was run:

```
$ sudo apt-get update
```
Then, all that is left is to make the installation
```
$ sudo apt-get install graphicsmagick
```
and GraphicsMagick should be ready for use.

### Using GraphicsMagick

#### Reducing image size
To start using this software, I chose to select a picture from my phone, from one of my favourite places in my hometown of Braga, [Bom Jesus do Monte](https://whc.unesco.org/en/list/1590/gallery/). It weighed 3.4MB, and had dimensions of 4407x3086 pixels.
To reduce its size, I used the following command:

```
gm convert -resize 298x206 braga.jpg braga_smaller.jpg
```
in which "298x206" is the converted size in pixels (both height and width were divided by 15), "braga.jpg" is the image I wanted to edit and "braga_smaller.jpg" is the image produced by GraphicsMagick, which appears in the current directory.

After generating this image, I made small horizontal cuts using a regular image editor (on Linux, Shotwell Viewer), such that the final image was 268x206. In the end, this picture occupied 23KB.


#### Creating a strip of various images
I decided to use another feature of GraphicsMagick as well: to make a strip of various photos, horizontally or vertically.
For this, I picked another photo I took on the same day, and edited it as preivously explained for the first photo. Then, I used the following command to create a single image with them:

```
gm convert -append -geometry 268x braga_smaller.jpg braga2_smaller.jpg braga_final.jpg
```
Here, "-geometry 268x" makes the final image 268 pixels in height, "braga_smaller.jpg" and "braga2_smaller.jpg" are the images to append and "braga_final.jpg" is the final result, which had a size of 30.7KB. It is shown below:

![](../images/braga_final.jpg)

Before creating this horizontal image strip, I tried other commands, which did not go as I wanted. Here, it is important to note that the two photos had slightly different widths (as can be seen in the image strip).

- with "+append" instead of "-append", the final image is vertical
```
gm convert +append -geometry 268x braga_smaller.jpg braga2_smaller.jpg braga_final.jpg
```
```
gm convert +append -geometry x268 braga_smaller.jpg braga2_smaller.jpg braga_final.jpg
```
Moreover, using "268x", the image with smaller width remains with the same size, so the software adds a small vertical white gap to fill the empty space. Using "x268", the white gap disappears and the smaller image just widens in order to fill the remaining space.

- with "x268" instead of "268x", the final image is still horizontal, but the same vertical line is created.
```
gm convert -append -geometry x268 braga_smaller.jpg braga2_smaller.jpg braga_final.jpg
```
With the final way used, both images conserve their original width.
