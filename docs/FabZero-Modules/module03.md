# 3. Computer controlled cutting

This module had 3 parts.
- The introduction (in class):
  - discussing security measures;
  - learning materials that can be used in a laser cutter;
  - learning what a kirigami is, and examples of them;
  - explanation on how to use Inkscape and the machine software.
- The group assignment: choosing a material available in the FabLab and testing the machine parameters to better cut and fold it;
- The individual assignment: creating a kirigami.

## Kirigami
Kirigami is a traditional janapanese art form, that uses folding and cutting paper in order to create 3D forms. It has also many applications in science and engeneering, and can be done using different materials and with the help of laser cutters, since these provide two useful functions:
- cutting with precision various types of materials,
- engraving materials, which can make them easier to bend, without breaking.

## Laser cutters in the FabLab

There are three laser cutters in the FabLab; the one I used for the individual assignment was the same one as for the group assignement: Epilog Fusion Pro 32, with the following parameters:
- Cutting surface: 81 x 50 cm
- Maximum height: 31 cm
- Laser power: 60 W
- Type of laser: CO2 tube (infrared)

## Group assignment - Epilog Fusion Pro 32
Our first assignment this week was to choose a material and determine the best parameters for our specific machine (speed, power, kerf, ...)

We will work with svg files (Scalable Vector Graphics). The program we will use is InkScape, but we could also use OpenSCAD.

### Create the test bench
The first step is to sketch the piece we want to cut. We decide to do a grid of 5x5 squares on InkScape.
To be able to use it with the laser cutter, we need to use a vectorial image. So we export it this way:

![](../images/vectorial.jpg)

Next, we need to color each square outline. Each one will have a different color because we want to be able to test each combination of force and speed at once.
IMAGE!!
We chose to use a gray gradient for our different colors.
We export our sketch into SVG. Using a USB key we open our file on the Epilog's computer.

### Cut the test bench (first try)
We open the file and we go to FILE>PRINT and we select the machine.

![](../images/fileprint.jpg)

IMAGE!!
Next, we get into the Epilog's Software. We need now to set the parameters.
First, pushing on the sketch, we have the possibility to split this sketch by colors. Then, we can determine the parameters for each subpart (each part of the sketch having a different color).
For each one, we choose those parameters :

**process type**: vector

**speed**: we go from 10 to 90 (step of 20)

**power**: we go from 10 to 90 (step of 20)

Next, we choose to go with orange cardboard. We put it into the laser cutter (careful to put it flat in the machine to keep a constant distance between the cardboard and the laser).
On the software, we check with the video how to place the sketch. After, we check that the borders are well placed (those borders defines the region the laser can go into).
We choose for the parameter Auto Focus : plunger. Then, we print to add the file to the Epilog machine.
SECURITY PHOTOS et MESURES (smoke, machine key, close)

![](../images/careful.jpg)

We choose the file on the Epilog machine and we start.

![](../images/start.jpg)

After having cut the piece, we can delete the file from the machine.

![](../images/test1.jpg)

The result is a bit disappointing, almost every square is simply totally cut through. It is simply because the cardboard is too thin and the laser set on a too high power.

### Cut the test bench (second try)
For our second try, we decided to focus ourself on a small part of the power range.

![](../images/drawing.jpg)

This time, the parameters are :

**process type**: vector

**speed**: we go from 30 to 90 (step of 15)

**power**: we go from 5 to 25 (step of 5)

![](../images/test2.jpg)

The result is much more conclusive. Some of the square we very loose and fell down as soon as we picked up the test bench, but we have much more samples to study. We can see that if we go too slow, it will inevitably cut through the paper.

### Cut the test bench (third try)
For the second part of the assignment, the Kirigamis, we still lack one important information, what is are the best parameters to be able to easily fold our cardboard without simply cutting it.
We designed a simple rectangle with 5 lines, each having different parameters. We chose those parameters based on our previous test bench, we selected the squares that were not cut through but still quite deeply cut.

![](../images/PLI.jpg)

The parameters are :

**process type**: vector

**speed**: 30, 60, 75, 90, 90

**power**: 5, 10, 10, 10, 15

Every cut hangs on correctly, but the best parameters pair seems to be (90,10), as it seems to be a bit more resilient.


## Individual Assignment

For the individual assignment, I chose to create a 3D geometric figure: a pyramid.

To do so, I made the following drawing in Inkscape:

![](../images/pyramid.png)

- the center square (and, therefore, the corresponding side of each triangle) was to be engraved,
- the outter two sides of each triangle were to be cut.

The square was created by using a rectangle in the pre-defined forms, removing its fill color and fixing both its width and lenght at **35 mm**.
The triangles required more steps, since I did not find them pre-defined. I therefore created two auxiliary lines (to be later removed), passing through the center of the square in the directions perpendicular to its sides. Then, using the "Edit paths by nodes" option in InkScape, the triangles were made joining the edges of the square to the desired height of the auxiliary lines, which was **33.6 mm**.

Since i chose the same material as in the group assignment, the cardboard, I used the second test bench produced to make a decision on the parameters for the laser cutter.

For the part to be cut, I picked **_45s 15p_** (45% speed and 15% power) and for engraving, I first chose **_90s 10p_**.

The result was decent, but the piece needed to be held together as shown in the picture below. Therefore, my task was to find the best parameters in order for the triangles tips to be the closest to each other, without me having to force them together with my hand.

![](../images/90_10.jpg)

For this, there was no need to change the parameters for the cutting, but to do so for the engraving (i.e., the square in the svg file).

- second try: **_90s 15p_**

My reasoning was that for higher power, the engraving would make the cardboard looser and therefore more subject to gravity.

The capacity for the object to hold improved very little, if anything, as can be seen in the images below.

![](../images/90_15.jpg)


- third try: **_60s 12p_**

Still thinking the stronger the cut, the easier it would be for the piece to hold, I chose higher power. Looking at the second test bench (group assignement), it can be seen that these parameters could have totally cut the cardboard, since the square _60s 10p_ is engraved but the _60s 15p_ is cut. Since _60s 12p_ was in between, I wanted to see if the pyramid held more strongly.

In practice, the engraving worked and the object was slightly better, but I decided to keep trying.

![](../images/60_12.jpg)


- fourth try: **_50s 10p_**

Here, I decided to lower the speed a bit more, but to make sure the engraving would not break, I lowered the power as well. The result was similar to the previous one.

- fifth try: **_75s 15p_**

In order to see a bigger difference, I picked one of the combinations of speed and power that was right next to combinations of speed and power that would cut the material, in the test bench.

In this case, the triangles were truly loose but that proved to work out poorly: one of them easily broke, and the others almost did too.

![](../images/75_15.jpg)

I did, however, realise this was not the best approach.

- final try: **_60s 5p_**

Here, I tried the opposite idea. I realised that if the engraving was softer, the cardboard could be less damaged in the folding boarders and could therefore hold stronger.

These parameters proved to work out better than the previous attempts, in fact.

The final result, shown below, still does not hold perfectly, but it is an improvement.

![](../images/60_5.jpg)
