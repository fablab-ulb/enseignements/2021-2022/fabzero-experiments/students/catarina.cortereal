# 2. Computer-Aided Design (CAD)

On this module, we learnt how to use software to make creations to be later printed 3D.
These softwares were OpenSCAD and FreeCAD. The first one is an open CAD modeller that has as inputs scripts, and outputs a 3D view of the project at hand. Moreover, FreeCAD allows for manipulation with the computer's mouse of the object, having a heavier graphical user interface.

## CAD of FlexLinks pieces

To test the use of CAD, a FlexLinks piece was designed in OpenSCAD, in a parametric way. The reason for this is that if the code is written in terms of lenghts, dephts, and other parameters instead of their actual values, these can be changed much more easily, when the printer's characteristics are known more precisely, for instance.

The license for the code below is [CC-BY](https://creativecommons.org/licenses/by/4.0/), the lest restrictive of the [Creative Commons License](https://creativecommons.org/about/cclicenses/). Its description is, very simply:
> This license allows reusers to distribute, remix, adapt, and build upon the material in any medium or format, so long as attribution is given to the creator. The license allows for commercial use.

The extracts of code below are, therefore, indexed the following way:

```
/*
    FILE   : fixed_slotted_beam.scad
    
    AUTHOR : Catarina Corte-Real <catarina.caldas.pereira.cortereal@ulb.be>
    
    DATE   : 2022-03-01
    
    LICENSE : Creative Commons Attribution [CC BY 4.0].
*/    

```

The piece chosen was the "fixed-slotted beam - straight".

Firstly, the **connecting part** between the pierced pieces is created, with the simple line

```
//connecting part
cube([lenght, c_h/2, c_h], true);
```
where _lenght_ is self-explanatory and _c_h_ is the height and depth of this wire, which will coincide with the height of all final and auxiliar pieces in the project.

The **piece with one large hole** is written in the code below, in which its lenght is controled by the quantity _x_transl1_. The FlexLinks piece was written such that, if the lenght of both pierced pieces were the same, the center of the wire would correspond to the point (0,0,0) in OpenSCAD. This is the reason for the first "translate" instruction, in which the quantity _dx_ is used in order to make sure the wire truly connects to this piece; it is to be changed as necessary for the warning "Object may not be a valid 2-manifold and may need repair! " to disappear in the rendering of the project.

This piece is made by computing the difference between the hull of two cylinders with radius _c_r_, which create an oval disk-like shape (first "hull" instruction), and the hole, created as a smaller disk using cylinders with a smaller radius, _smallc_r_ (second "hull" instruction).
```
//piece with one large hole
translate([lenght/2 + x_transl + dx,0,0])difference(){
    hull(){
        translate([-x_transl1/2,0,0])cylinder(c_h, c_r, c_r, true);
        translate([x_transl1/2,0,0])cylinder(c_h, c_r, c_r, true);}
    hull(){
        translate([-x_transl1/2,0,0])cylinder(c_h, smallc_r, smallc_r, true);
        translate([x_transl1/2,0,0])cylinder(c_h, smallc_r, smallc_r, true);}
    }
```
Finally, only the **piece with two small holes** is missing. It is created, as in the previous piece, by making a larger disk, and computing its difference with two smaller  circular ones, by creating for each of them two cylinders with radius _smallc_r1_.

The position of this piece is computed in a similar way to the other pierced piece. The need for both _transl_ and _transl1_ stems from the possibility of the pierced pieces having different lenghts.
```
//piece with two small holes
translate([-(lenght/2 + x_transl - dx),0,0])difference(){
    hull(){
        translate([-x_transl/2,0,0])cylinder(c_h, c_r, c_r, true);
        translate([x_transl/2,0,0])cylinder(c_h, c_r, c_r, true);}
    translate([-x_transl/2,0,0])cylinder(c_h, smallc_r1, smallc_r1, true);
    translate([x_transl/2,0,0])cylinder(c_h, smallc_r1, smallc_r1, true);
    }
```
All these three parts of the final product are encoded in a "union", to further guarantee they are whole in one. A line of code

```
$fn=150
```
is also put at the beggining of the code, in order to soften the borders of the object, that can be seen below.

![](../images/openscad_piece.png)

The values of the parameters used preliminarily to see a draft of the object on OpenSCAD are as follows:
```
c_h=2;
c_r=3;
lenght=40;
smallc_r=2;
smallc_r1=1.3;
x_transl=6;
x_transl1=9;
dx=0.6;
```
To create an easier guide on how to manipulate the parameters in this project, a quick summary is made.
To control:

 - the connecting part's lenght, change the value of _lenght_;
 - the height of the whole object, change _c_h_;
 - the thickness of the borders of both pieces, change _c_r_;
 - the size of the hole in the one large hole piece, change _smallc_r_;
 - the size of both small holes in the other piece, change _smallc_r1_;
 - the lenght of the one-hole piece, change _x_transl1_;
  - the lenght of the two-hole piece, change _x_transl_;
 - if the 3 parts are correctly connected, change _dx_.
